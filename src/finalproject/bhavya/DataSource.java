package finalproject.bhavya;

import java.util.ArrayList;

/**
 * @author Bhavya
 * 
 *         A DataSource interface which has one abstract method called
 *         getRunners()
 */
public interface DataSource {
	public abstract ArrayList<ThreadRunner> getRunners();
}
