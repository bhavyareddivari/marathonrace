package finalproject.bhavya;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * @author Bhavya This is the RunnersXMLFile class which implements DataSource
 *         interface used to read runners data from an XML file.
 */
public class RunnersXMLFile implements DataSource {
	// creating scanner object
	Scanner sc = new Scanner(System.in);

	/**
	 * Get ArrayList object which contains ThreadRunner objects.
	 * 
	 * @return runners is an arraylist which contains ThraedRunner objects
	 */
	public ArrayList<ThreadRunner> getRunners() {

		String name = "";
		int speed = 0;
		int restPercentage = 0;
		ArrayList<ThreadRunner> runners = new ArrayList<>();
		String fileName = Validator.getRequiredString(sc, "Enter XML file name: ");
		File file = new File("Resources/" + fileName);
		// creating new instance of the XMLInputFactory
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		try {
			// Parsing XML elements
			XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(file));
			while (xmlEventReader.hasNext()) {
				XMLEvent xmlEvent = xmlEventReader.nextEvent();
				// check if the event is start element
				if (xmlEvent.isStartElement()) {
					StartElement startElement = xmlEvent.asStartElement();
					if (startElement.getName().getLocalPart().equals("Runner")) {
						// Get the 'name' attribute from Runner element
						Attribute idAttr = startElement.getAttributeByName(new QName("Name"));
						if (idAttr != null) {
							name = idAttr.getValue();
						}
					}
					// set the other varibles from xml elements
					else if (startElement.getName().getLocalPart().equals("RunnersMoveIncrement")) {
						xmlEvent = xmlEventReader.nextEvent();
						speed = Integer.parseInt(xmlEvent.asCharacters().getData());

					} else if (startElement.getName().getLocalPart().equals("RestPercentage")) {
						xmlEvent = xmlEventReader.nextEvent();
						restPercentage = Integer.parseInt(xmlEvent.asCharacters().getData());
					}
				}
				// if Runner end element is reached, add Runner object to
				// arraylist.
				if (xmlEvent.isEndElement()) {
					EndElement endElement = xmlEvent.asEndElement();
					if (endElement.getName().getLocalPart().equals("Runner")) {
						try {
							ThreadRunner runner = new ThreadRunner(name, speed, restPercentage);
							runners.add(runner);
						} catch (Exception e) {
						}
					}
				}
			}

		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
		return runners;
	}
}