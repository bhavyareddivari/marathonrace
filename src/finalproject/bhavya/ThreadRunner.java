package finalproject.bhavya;

/**
 * @author Bhavya
 * 
 *         ThreadRunner class which extends Thread Class
 *
 */
class ThreadRunner extends Thread {
	// Declaring instance variables
	public static final int MARATHON_DISTANCE = 1000;
	private String runnersName;
	private int restPercentage;
	private int runnersSpeed;

	/**
	 * Constructor
	 * 
	 * @param runnersName
	 *            is the name of the runner
	 * @param runnersSpeed
	 *            is the speed of the runner
	 * @param restPercentage
	 *            is the rest percentage of the runner
	 */
	public ThreadRunner(String runnersName, int runnersSpeed, int restPercentage) {
		this.runnersName = runnersName;
		this.restPercentage = restPercentage;
		this.runnersSpeed = runnersSpeed;
	}

	/**
	 * Getter method for name of the runner
	 * 
	 * @return runnerName which is the name of the runner
	 */
	public String getRunnersName() {
		return runnersName;
	}

	/*
	 * Overriding run method of Thread class
	 */
	@Override
	public void run() {
		int distance = 0;
		//while (!isInterrupted() && distance <= 1000) {
		while(!isInterrupted() && distance <= 1000){
			try {
				// getting random number
				int random = (int) (Math.random() * 100);
				synchronized (getClass()) {	//synchronized block
					if (isInterrupted()) {
						break;
					}
					if (restPercentage <= random) {
						distance += runnersSpeed; // adding speed to distance
						System.out.println(runnersName + " : " + distance);
						if (distance >= 1000) {
							System.out.println(runnersName + ": I finished!" + "\n");
							MarathonRaceApp.finished(Thread.currentThread(), runnersName);
							break;
						}
					}
				}
				Thread.sleep(100);	//sleep thread for 100ms
			} catch (InterruptedException e) {
				break;
			}
		}

	}

}
