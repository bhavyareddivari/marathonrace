package finalproject.bhavya;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Bhavya
 * 
 *         This is the RunnersTextFile class which implements DataSource
 *         interface.
 */
public class RunnersTextFile implements DataSource {
	// Declaring instance variables
	private BufferedReader bufferedReader = null;
	Scanner sc = new Scanner(System.in);

	/**
	 * Constructor
	 *
	 * @throws Exception
	 *             is thrown when the specified file is not found.
	 */
	public RunnersTextFile() throws Exception {
		String filename = Validator.getRequiredString(sc, "Enter text file name: ");
		File file = new File("Resources/" + filename);
		try {
			// reading text file using BufferedReader
			bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		} catch (FileNotFoundException e) {
			System.out.println("File: " + filename + " is not found.");
		}
		// If file doesnot exists, throw exception
		if (bufferedReader == null) {
			throw new Exception();
		}
	}

	/**
	 * Get ArrayList object which contains ThreadRunner objects.
	 * 
	 * @return runners is an arraylist which contains ThraedRunner objects
	 */
	public ArrayList<ThreadRunner> getRunners() {
		ArrayList<ThreadRunner> runners = new ArrayList<ThreadRunner>();
		try {
			String line;
			// Reading all runners stored in file and storing in array list.
			while ((line = bufferedReader.readLine()) != null) {
				ThreadRunner runner = parse(line);
				if (runner != null)
					runners.add(runner);
			}
		} catch (IOException e) {
			System.err.println("Error reading input text file.");
		}
		return runners;
	}

	/**
	 * This method creates a ThreadRunner instance from a line of text;
	 *
	 * @param line
	 *            is a line from the input text file.
	 * @returns ThreadRunner instance
	 */
	private ThreadRunner parse(String line) {
		String[] columns = line.split(" ");
		try {
			String name = columns[0];
			int speed = Integer.parseInt(columns[1]);
			int restPercentage = Integer.parseInt(columns[2]);
			return new ThreadRunner(name, speed, restPercentage);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}
