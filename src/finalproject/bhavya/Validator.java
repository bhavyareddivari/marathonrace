package finalproject.bhavya;

import java.util.Scanner;

/**
 * @author Bhavya
 * 
 *         This class is used to validate the user input
 */
public class Validator {

	/**
	 * Method to validate string entered by user
	 * 
	 * @param sc
	 *            is the scanner object
	 * @param prompt
	 *            is the message displayed in console
	 * @return String entered by user
	 */
	public static String getRequiredString(Scanner sc, String prompt) {
		String s = "";
		boolean isValid = false;
		while (isValid == false) {
			System.out.print(prompt);
			s = sc.nextLine();
			if (s.equals("")) {
				System.out.println("Error! This entry is required. Try again.");
			} else {
				isValid = true;
			}
		}
		return s;
	}

	/**
	 * Method to validate Integer entered by user
	 * 
	 * @param sc
	 *            is the scanner object
	 * @param prompt
	 *            is the message displayed in console
	 * @return integer entered by user
	 */
	public static int getInteger(Scanner sc, String prompt) {
		int i = 0;
		boolean isValid = false;
		while (isValid == false) {
			System.out.print(prompt);
			if (sc.hasNextInt()) {
				i = sc.nextInt();
				isValid = true;
			} else {
				System.out.println("Error! Invalid integer value. Try again.");
			}
			sc.nextLine(); // discard any other data entered on the line
		}
		return i;
	}

	/**
	 * Method to validate the integer entered by user within range
	 * 
	 * @param sc
	 *            is the scanner object
	 * @param prompt
	 *            is the message displayed in console
	 * @param min
	 *            is the minimum value required
	 * @param max
	 *            is the maximum value required
	 * @return Integer entered by user
	 */
	public static int getIntegerWithinRange(Scanner sc, String prompt, int min, int max) {
		int i = 0;
		boolean isValid = false;
		while (isValid == false) {
			i = getInteger(sc, prompt);
			if (i < min)
				System.out.println("Error! Number must be greater than " + min + ".");
			else if (i > max)
				System.out.println("Error! Number must be less than " + max + ".");
			else
				isValid = true;
		}
		return i;
	}

}
