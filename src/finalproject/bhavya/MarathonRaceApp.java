package finalproject.bhavya;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author Bhavya
 * 
 *         MarathonRaceApp class
 *
 */
public class MarathonRaceApp {
	// Declaring instance variables
	public static ArrayList<ThreadRunner> runningThreads = new ArrayList<>();
	Scanner sc = new Scanner(System.in);

	/**
	 * Main method
	 * 
	 * @param args
	 *            are not used here.
	 */
	public static void main(String[] args) {
		new MarathonRaceApp().getSource();
	}

	/**
	 * The getSource method shows welcome message, displays menu and get Source
	 * data from one of the option user selects.
	 */
	public void getSource() {
		// local variable
		String ch;
		// welcome message
		System.out.println("Welcome to the Marathon Race Runner Program\n");
		// Loop continues until user wants to exit
		do {
			showMenu();
			int choice = Validator.getIntegerWithinRange(sc, "Enter your choice: ", 1, 5);
			DataSource source;
			switch (choice) {
			// If user wants to read runners data from database
			case 1:
				try {
					source = new RunnersDB(); // create object of RunnersDB
												// class
					runningThreads = source.getRunners();
				} catch (Exception e) {
				}
				break;
			// If user wants to read runners data from XML file
			case 2:
				try {
					source = new RunnersXMLFile(); // create object of
													// RunnersXMLFile class
					runningThreads = source.getRunners();
				} catch (Exception e) {
				}
				break;
			// If user wants to read runners data from Text file
			case 3:
				try {
					source = new RunnersTextFile(); // Create object of
													// RunnersTextFile class
					runningThreads = source.getRunners();
					// continueFlag = true;
				} catch (Exception e) {
				}
				break;
			// If user wants to read runners data from default data provided
			case 4:
				// adding data to arraylist
				runningThreads.add(new ThreadRunner("Tortoise", 10, 0));
				runningThreads.add(new ThreadRunner("Hare", 100, 90));
				break;
			// If user wants to exit
			case 5:
				System.out.println("\nThanks for using my Marathon Race Program");
				System.exit(0);
			default:
				System.out.println("Invalid choice.");
				break;
			}
			// check if there are no runners
			if (runningThreads.size() == 0) {
				System.err.println("There are no runners!");
			} else {
				runThreads();
				runningThreads.clear();
			}
			System.out.print("\nPress any key to continue . . .");
			ch = sc.nextLine();
		} while (ch != null);
	}

	/**
	 * Show Menu
	 */
	private void showMenu() {
		System.out.println("Select your data source:\n");
		System.out.println("1. Derby database");
		System.out.println("2. XML file");
		System.out.println("3. Text file");
		System.out.println("4. Default two runners");
		System.out.println("5. Exit\n");
	}

	/**
	 * Runs ThreadRunner objects
	 */
	private void runThreads() {

		System.out.println("Get... set... Go!");
		// starting all threads
		for (int i = 0; i < runningThreads.size(); i++) {
			runningThreads.get(i).start();
		}
		for (int i = 0; i < runningThreads.size(); i++) {
			try {
				runningThreads.get(i).join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * Shows results after the race has finished
	 * 
	 * @param winner
	 *            is the thread which has won the race.
	 * @param winnerName
	 *            is the name of the winner
	 */
	public synchronized static void finished(Thread winner, String winnerName) {
		// Declaring winner and interrupting other runners
		for (int i = 0; i < runningThreads.size(); i++) {
			if (winnerName.equals(runningThreads.get(i).getRunnersName())) {
				System.out.println("The race is over! The " + winnerName + " is the winner" + "\n");
			} else
				runningThreads.get(i).interrupt();
		}
		// If not a winner, print message
		for (int i = 0; i < runningThreads.size(); i++) {
			if (!winnerName.equals(runningThreads.get(i).getRunnersName())) {
				System.out.println(runningThreads.get(i).getRunnersName() + ": You beat me fair and square.");

			}
		}
	}

}