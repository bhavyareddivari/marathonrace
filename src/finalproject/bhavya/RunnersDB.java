package finalproject.bhavya;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Bhavya
 * 
 *         This RunnersDB class implements DataSource interface which reads
 *         runners data from derby database.
 *
 */
public class RunnersDB implements DataSource {

	/**
	 * This method is used to establish a connection with derby database.
	 * 
	 * @return connection
	 */
	private static Connection getConnection() {
		// set the derby home directory
		String dbDirectory = "Resources";
		System.setProperty("derby.system.home", dbDirectory);

		// set the db url, username, and password
		String url = "jdbc:derby:FinalDB";
		String username = "";
		String password = "";

		try {
			Connection connection = DriverManager.getConnection(url, username, password);
			return connection;
		} catch (SQLException e) {
			System.err.println("SQLException: " + e);
			return null;
		}
	}

	/**
	 * Get ArrayList object which contains ThreadRunner objects.
	 * 
	 * @return runners is an Arraylist which contains ThraedRunner objects
	 */
	public ArrayList<ThreadRunner> getRunners() {

		ArrayList<ThreadRunner> runners = new ArrayList<>();
		// writing query to get data from the table
		String query = "SELECT * FROM RunnersStats";

		try (Connection connection = getConnection();
				PreparedStatement ps = connection.prepareStatement(query);
				ResultSet rs = ps.executeQuery()) {
			// Reads all information in the RunnersStats table and storing in
			// ArrayList
			while (rs.next()) {
				String name = rs.getString(1);
				int speed = rs.getInt(2);
				int restPercentage = rs.getInt(3);
				ThreadRunner runner = new ThreadRunner(name, speed, restPercentage);
				runners.add(runner);
			}
			return runners;
		} catch (Exception e) {
			return null;
		}
	}
}
